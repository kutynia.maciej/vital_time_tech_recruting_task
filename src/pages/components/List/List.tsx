import React from 'react';
import ListItem from "./components/ListItem";
import useList from "@/pages/components/List/useList";

const List = () => {

    const {
        dragStartHandler,
        dragEndHandler,
        dragEnterHandler,
        dragLeaveHandler,
        dropHandler,
        dragOverHandler,
        draggingIndex,
        list
    } = useList()


    return (
        <ul>
            {list.map((item, index) => (
                <ListItem key={item.id} draggingIndex={draggingIndex} {...item} onDragStart={dragStartHandler}
                          onDragEnd={dragEndHandler}
                          onDragEnter={dragEnterHandler} onDrop={dropHandler} onDragOver={dragOverHandler}
                          onDragLeave={dragLeaveHandler}/>
            ))}
        </ul>
    );
}

export default List;