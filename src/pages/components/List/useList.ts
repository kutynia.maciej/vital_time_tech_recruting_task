import {DragEvent, useState} from "react";
import {listItems} from "@/pages/api/list-data";

const useList = () => {

    const draggingClass = 'dragging'
    const draggingElement = 'dragging_element'

    const [list, setList] = useState(listItems);
    const [draggingIndex, setDraggingIndex] = useState<number | null>(null);
    const [draggingDirection, setDraggingDirection] = useState('')

    const dragStartHandler = (e: DragEvent<HTMLLIElement>) => {
        const target = e.target as HTMLLIElement;
        target.classList.add(draggingClass);
        setDraggingIndex(Number(target.id))
    }

    const dragEndHandler = (e: DragEvent<HTMLLIElement>) => {
        const target = e.target as HTMLLIElement
        target.classList.remove(`${draggingElement}_top`)
        target.classList.remove(`${draggingElement}_bottom`)
        target.classList.remove(draggingClass)
    }

    const dragEnterHandler = (e: DragEvent<HTMLLIElement>) => {
        const target = e.target as HTMLLIElement
        if (target.classList.contains('list_item') && !target.classList.contains(draggingClass)) {
            target.classList.add(draggingElement)
        }
    }

    const dragOverHandler = (e: DragEvent<HTMLLIElement>) => {
        const target = e.target as HTMLLIElement
        e.preventDefault();
        const boundingRect = target.getBoundingClientRect();
        const offsetY = e.clientY - boundingRect.top;
        if (offsetY < boundingRect.height / 2) {
            target.classList.remove(`${draggingElement}_bottom`)
            if (!target.classList.contains(`${draggingElement}_top`)) {
                target.classList.add(`${draggingElement}_top`)
            }
            if (draggingDirection !== 'top') {
                setDraggingDirection('top')
            }
        } else {
            target.classList.remove(`${draggingElement}_top`);
            if (!target.classList.contains(`${draggingElement}_bottom`)) {
                target.classList.add(`${draggingElement}_bottom`)
            }
            if (draggingDirection !== 'bottom') {
                setDraggingDirection('bottom')
            }
        }
    }

    const dragLeaveHandler = (e: DragEvent<HTMLLIElement>) => {
        const target = e.target as HTMLLIElement
        target.classList.remove(`${draggingElement}_top`)
        target.classList.remove(`${draggingElement}_bottom`)

    }

    const dropHandler = (e: DragEvent<HTMLLIElement>) => {
        const target = e.target as HTMLLIElement
        setList(list => {
            const arr = [...list]
            const fromIndex = arr.findIndex(el => el.id === draggingIndex)
            const el = arr.splice(fromIndex, 1)?.[0]
            const toIndex = Number(target.id);
            arr.splice(draggingDirection === 'top' ? toIndex - 1 : toIndex, 0, el)
            return arr
        })
        target.classList.remove(`${draggingElement}_top`)
        target.classList.remove(`${draggingElement}_bottom`)
        setDraggingIndex(null)
    }


    return {
        dragStartHandler,
        dragEndHandler,
        dragEnterHandler,
        dragLeaveHandler,
        dropHandler,
        dragOverHandler,
        draggingIndex,
        list
    }
}

export default useList;