import React from 'react';
import Image from "next/image";
import {ListItemProps} from "@/interfaces/ListItem";

const ListItem = (props: ListItemProps) => {
    const {thumbnail, place, name, id, draggingIndex, ...handlers} = props;

    return (
        <li id={id.toString()} draggable {...handlers}
            className={`px-10 py-5 bg-white min-w-[35rem] font-gelion items-center gap-6 flex list_item ${draggingIndex === id ? 'opacity-40 border-t-[#F4F5F6] border-b-[#F4F5F6] bg-[#F4F5F6]' : ''}`}>
            <div
                className='w-24 h-24 overflow-hidden rounded-xl relative object-contain img-box select-none pointer-events-none'>
                <Image src={thumbnail} alt={name} fill={true}
                       className='select-none pointer-events-none'/>
            </div>
            <div className='pointer-events-none'>
                <p className='font-medium pointer-events-none'>{name}</p>
                <p className='flex gap-2 items-center pointer-events-none place'>
                    <Image src='/images/union.png' alt='union' width={11} height={15} className='h-[15px]'/>
                    <span>{place}</span>
                </p>

            </div>
        </li>
    )
}

export default ListItem;