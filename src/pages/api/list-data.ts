export const listItems = [
    {
        id: 1,
        name: 'Scotland Island',
        place: 'Sydney, Australia',
        thumbnail: '/images/img_1.png'
    },
    {
        id: 2,
        name: 'The Charles Grand Brasserie & Bar',
        place: 'Lorem ipsum, Dolor',
        thumbnail: '/images/img_2.png'
    },
    {
        id: 3,
        name: 'Bridge Climb',
        place: 'Dolor, Sit amet',
        thumbnail: '/images/img_3.png'
    },
    {
        id: 4,
        name: 'Scotland Island',
        place: 'Sydney, Australia',
        thumbnail: '/images/img_4.png'
    },
    {
        id: 5,
        name: 'Clam Bar',
        place: 'Etcetera veni, Vidi vici',
        thumbnail: '/images/img_5.png'
    },
    {
        id: 6,
        name: 'Vivid Festival',
        place: 'Sydney, Australia',
        thumbnail: '/images/img_6.png'
    },
]