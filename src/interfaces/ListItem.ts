import {DragEvent} from "react";

export interface ListItemProps {
    name: string,
    thumbnail: string,
    place: string
    id: number
    draggingIndex: number | null
    onDragStart: (e: DragEvent<HTMLLIElement>) => void;
    onDragLeave: (e: DragEvent<HTMLLIElement>) => void;
    onDragOver: (e: DragEvent<HTMLLIElement>) => void;
    onDrop: (e: DragEvent<HTMLLIElement>) => void;
    onDragEnd: (e: DragEvent<HTMLLIElement>) => void;
    onDragEnter: (e: DragEvent<HTMLLIElement>) => void;
}